// Trilinos includes
#include "Epetra_CrsMatrix.h"
#include "Epetra_MultiVector.h"
#include "Epetra_Export.h"
#include "Epetra_LinearProblem.h"
#include "Galeri_Maps.h"
#include "Galeri_CrsMatrices.h"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Ifpack.h"
#include "Ifpack_AdditiveSchwarz.h"
#include "BelosLinearProblem.hpp"
#include "BelosBlockGmresSolMgr.hpp"
#include "BelosEpetraAdapter.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Teuchos_oblackholestream.hpp"
#include "Teuchos_FileInputSource.hpp"
#include "Teuchos_XMLParameterListReader.hpp"

// MPI
#ifdef HAVE_MPI
#include "Epetra_MpiComm.h"
#else
#include "Epetra_SerialComm.h"
#endif

// STD
#include <string>
#include <iostream>
#include <random>
#include <vector>

// Eigen
#include "Eigen/Sparse"

// e62numcodes for access to the HDF5 wrapper
#include <io.hpp>
#include <common.hpp>

namespace {
    // set to false to disable debugging prints to console
    constexpr bool DEBUG_OUTPUT = true;
}


using std::string;
using std::cout;
using std::endl;
using Teuchos::ParameterList;
using Teuchos::RCP;				// reference counting pointer type
using Teuchos::rcp;				// reference counting pointer adapter function


#ifdef EPETRA_NO_32BIT_GLOBAL_INDICES
    typedef long long global_ordinal_type;
#else
    typedef int global_ordinal_type;
#endif // EPETRA_NO_32BIT_GLOBAL_INDICES


#ifdef HAVE_MPI
    typedef Epetra_MpiComm Communicator;
    #define COMMUNICATOR_INIT_ARGS MPI_COMM_WORLD
#else
    typedef Epetra_SerialComm Communicator;
    #define COMMUNICATOR_INIT_ARGS
#endif



/**
 * @brief Reads a sparse matrix from a hdf5 folder (group). Matrix M is assumed to be stored
 * as an int vector columns, int vector rows and double vector values
 * @warning By default, the indexes of the matrix elements are loaded as in 0-based format.
 * @param reader the HDF5 reader instance, with group already loaded
 * @param one_based Whether the indices are stored as one-based and have to be converted to zero based form.
 * @return sparse matrix A.
 */
Eigen::SparseMatrix<double> getSparseMatrixMultiVar(mm::HDF5IO& reader, bool one_based = false) {
    // get triplets out of the file the easy way
    typedef Eigen::Triplet<double> Tri;
    std::vector<Tri> eigenTriplets(0);

    int nx = 0, ny = 0;
    int mx = 999999, my = 999999;

    // convert triplets to Eigen sparse matrix
    try {
		
        std::vector<int> columns = reader.getIntArray("columns");
        std::vector<int> rows = reader.getIntArray("rows");
        std::vector<double> values = reader.getDoubleArray("values");
        eigenTriplets.reserve(columns.size());
		
		for (size_t i=0; i < columns.size(); ++i) { 
            if (one_based) {
                --columns[i];
                --rows[i];
            }
            eigenTriplets.push_back(Tri(columns[i], rows[i], values[i]));
			//eigenTriplets.push_back(Tri(columns[i], rows[i], 1));
            if (columns[i] > ny) ny = columns[i];
            if (rows[i] > nx) nx = rows[i];
            if (columns[i] < my) my = columns[i];
            if (rows[i] < mx) mx = rows[i];
        }
		
    } catch (std::exception& e) {
        std::cerr << "Exception " << e.what();
        return {};
    }
    
	// DEBUG DEBUG
    //std::cout << "reading complete, "<< ny << " rows, " << nx << " colums, min = " << my << ", " << mx << "; now converting to Eigen::Sparse\n";
        
    Eigen::SparseMatrix<double> matrix(ny+1, nx+1);
    // triplets were successfully loaded, commence conversion
    
    try {
		matrix.setFromTriplets(eigenTriplets.begin(), eigenTriplets.end());
        //for (size_t i=0; i < columns.size(); ++i) { 
        //    matrix.insert(rows[i], columns[i]) = values[i];
        //}
    } catch (std::exception& e) {
        std::cerr << e.what();
    }
    
    if (DEBUG_OUTPUT)
        std::cerr << "   loaded Eigen matrix " << std::to_string(matrix.cols())<<"x"<<std::to_string(matrix.rows())<< "\n";
    return matrix;
}


/**
 * @brief Reads a sparse matrix from a hdf5 folder (group). Matrix M is assumed to be stored
 * by the setSparseMatrix() function - as vectors I, J, V, such that M(I(i), J(i)) = V(i).
 * @warning By default, the indexes of the matrix elements are converted from 1-based format.
 * @param reader the HDF5 reader instance, with group already loaded
 * @param name Attribute name.
 * @param one_based Whether the indices are stored as one-based and have to be converted to zero based form.
 * @return sparse matrix A.
 */
Eigen::SparseMatrix<double> getSparseMatrix(mm::HDF5IO& reader, const std::string& name, bool one_based = true) {
    // get triplets out of the file the easy way
    std::vector<std::vector<double>> triplets = reader.getDouble2DArray(name);
    unsigned int n = triplets.size();
    typedef Eigen::Triplet<double> Tri;
    std::vector<Tri> eigenTriplets(0);

    int nx = 0, ny = 0;

    // convert triplets to Eigen sparse matrix
    try {
        eigenTriplets.reserve(n);
        for (auto it = triplets.begin(); it != triplets.end(); ++it) {
			int col=it->at(0)-one_based;
			int row=it->at(1)-one_based;
            eigenTriplets.push_back(Tri(col, row, it->at(2)));
            if (col > ny) ny = col;
            if (row > nx) nx = row;
        }
        // empty the first vector to clear memory
        triplets.shrink_to_fit();
    } catch (std::exception& e) {
        return {};
    }

    // triplets were successfully loaded, commence conversion
    Eigen::SparseMatrix<double> matrix(ny+1, nx+1);
    matrix.setFromTriplets(eigenTriplets.begin(), eigenTriplets.end());

    if (DEBUG_OUTPUT)
        std::cerr << "   loaded Eigen matrix " << std::to_string(matrix.cols())<<"x"<<std::to_string(matrix.rows())<<"\n";
    return matrix;
}


class Solver {
    // typedefs for Eigen and Trilinos
    typedef Eigen::SparseMatrix<double>         ESMatrix;
    typedef Eigen::SparseMatrix<double, Eigen::RowMajor> RowMatrix;
    typedef Epetra_MultiVector                  MV;
    typedef Epetra_Operator                     OP;

    // Trilinos MPI communication devices and variables
    Communicator Comm;
    int myRank;                                 // defined in ctor
    int numProcesses;							// defined in ctor
    int numMyElements;
    global_ordinal_type numGlobalElements = -1; // -1 = undefined
    global_ordinal_type* myGlobalElements = nullptr;

    // zero-based indexing is enabled by providing constant 0 to some trilinos funcitons
    const global_ordinal_type indexBase = 0;

    // matrices and vectors for the solver
    RCP<Epetra_CrsMatrix> A;
    RCP<Epetra_MultiVector> RHS;
    RCP<Epetra_MultiVector> LHS;

    // output stream that is defined for MPI rpocess 0 only and ia a blackhole for other processes
    Teuchos::oblackholestream blackhole;
    std::ostream &out;

    // timers for Trilinos functional blocks
    RCP<Teuchos::Time> initTime;
    RCP<Teuchos::Time> randTime;
    RCP<Teuchos::Time> matrixImportTime;
    RCP<Teuchos::Time> solveTime;

    // XML files to load parameter lists form
    const char* const paramListFname = "Params.xml";
    ParameterList List;

public:
    /**
     * Initialize only the vars that have trivial constructors
     * - communicator (MPI or otherwise)
     * - output stream for MPI process zero
     * - timers
    **/
    Solver()  : Comm(COMMUNICATOR_INIT_ARGS), myRank(Comm.MyPID()), out(myRank == 0 ? std::cout : blackhole) {
		std::cerr << "   ctor on process " << myRank << "\n";
        numProcesses = Comm.NumProc();

        initTime = Teuchos::TimeMonitor::getNewCounter ("Solver::initialization");
        randTime = Teuchos::TimeMonitor::getNewCounter ("Solver::matrix randomization");
        matrixImportTime = Teuchos::TimeMonitor::getNewCounter ("Solver::matrix import");
        solveTime = Teuchos::TimeMonitor::getNewCounter ("Solver::solve time");
    }

    /**
     * Initialize all the variables, using commandline arguments:
     *  - sparse matrix A, distributed across processes
     *      - either  randomize and fill in values of A
     *      - or      call function to load A and RHS from hdf5 file
     *
     * Output some stats:
     *  - number of processes
     *
     * Commandline:
     *  - argument 1: square root of the number of elements in the matrix (nx), default is 100 (deprecaetd) or filename of hdf5 file with domain nodes
	 *  - argument 2: group/folder within the input hdf5 file, where variables defining the matrix are found ()
    **/
    int init(int argc, char *argv[]) {
        // start timing initialization
        Teuchos::TimeMonitor LocalTimer (*initTime);
        if (myRank == 0)
            std::cerr << "Using MPI on " << Comm.NumProc() << " processors\n";

        // read the parameters from XML file
        readParameterListFromFile(paramListFname, List);
        out << "Loading parameters from " << paramListFname << ":\n";
        List.print(out, Teuchos::ParameterList::PrintOptions().indent(2).showTypes(true));

        // The problem is defined on a 2D grid, global size is nx * nx.
        int nx = -1;
        // filename as read from the commandline
        std::string fnameCommandline;
        // folder as read from the commandline
        std::string h5folderCommandline;

        // parse commandline arguments:
        // if first argument is a number, then set nx = first argument
        // if first number is a string, then set input file name = first argument, input folder name = second argument
        if (argc > 1) {
            try {
                // is 1st parameter a number?
                nx = std::stoi(argv[1]);
            } catch (...) {
                // or a string (filename)
                fnameCommandline = argv[1];
                nx = -1;
                h5folderCommandline = (argc > 2 ? argv[2] : "/");
            }
        }

        // depending on weather a number was provided on the commandline or a file name, perform the following:
        if (nx > 0) {
            // a number was provided -> create a random matrix

            if (DEBUG_OUTPUT)
                out << "Domain of " << nx << " x " << nx << "\n";

            // MD ustvarjal malo po svoje...
            numGlobalElements = nx*nx;
            allocateDistributedSystem();
            // fill A with random numbers
            randomizeA();
        } else {
            // a string was provided, try to open it as hdf5 file
            ParameterList list(List.sublist("HDF5 Input"));
            std::string fname(list.get("File", "input.h5"));
            std::string dirName(list.get("Directory", "/"));
            std::string matrixVarName(list.get("System Matrix Variable Name", "M"));
            std::string rhsVarName(list.get("RHS Variable Name", "rhs"));

            // commandline arguments override the xml
            if (!h5folderCommandline.empty())
                dirName = h5folderCommandline;
            if (!fnameCommandline.empty())
                fname = fnameCommandline;

            if (DEBUG_OUTPUT)
                out << "Loading " << matrixVarName << " and " << rhsVarName << " from " << fname << "<" << dirName << ">\n";
            loadSystem(fname, dirName, matrixVarName, rhsVarName);

        }

        return 0;
    }


    /**
     * Create a system (matrix A, right hand side RHS, left hand side LHS) distributed across MPI processes
     */
    void allocateDistributedSystem() {
        // map for process distribution
        Epetra_Map map (numGlobalElements, indexBase, Comm);

        // initialize variables for the elements that belong to this particular process
        numMyElements = map.NumMyElements ();       // num of elements per processor?
        if (DEBUG_OUTPUT)
            out << " number of rows on one processor = " << numMyElements << "\n";

#ifdef EPETRA_NO_32BIT_GLOBAL_INDICES
        myGlobalElements = map.MyGlobalElements64 ();
#else
        myGlobalElements = map.MyGlobalElements ();
#endif // EPETRA_NO_32BIT_GLOBAL_INDICES

        if (numMyElements > 0 && myGlobalElements == NULL)
            throw std::logic_error ("Failed to get the list of global indices");

        // create A but do not fill it in yet
        A =   rcp(new Epetra_CrsMatrix(Copy, map, 25));         // used to be RCP<Epetra_CrsMatrix> = ...
        LHS = rcp(new Epetra_MultiVector(map, 1));
        LHS->PutScalar(1.0);
        RHS = rcp(new Epetra_MultiVector(map, 1)); //A->OperatorDomainMap ()
        LHS->Random();
    }

    /**
     * Load the matrix and rand hand side vector from h5 file
     *  - sparse matrix A, distributed across processes
     *  - right hand side as a vector
    **/
    int loadSystem(const std::string& h5Fname, const std::string& h5folder, const std::string& matrixName, const std::string& rhsName) {
        // start timing initialization
        Teuchos::TimeMonitor LocalTimer (*matrixImportTime);

        // prepare the structures to be loaded from file (Eigen sparse matrix and std vector)
        Eigen::SparseMatrix<double> matrix;
        std::vector<double> rhs;
        int numElements = 0;

        // laod matrix and right hand side from h5 file on rank 0 only
        if (myRank == 0) {
            if (DEBUG_OUTPUT)
                out << "Loading the system from file " << h5Fname << "\n";
            mm::HDF5IO reader(h5Fname, mm::HDF5IO::READONLY);
            reader.openFolder(h5folder);
            if (DEBUG_OUTPUT)
                out << "   reading matrix\n";

            if ((matrixName.size() > 1) && (matrixName[0] == '/')) {
                reader.openFolder(h5folder+matrixName);
                try {
                    matrix = getSparseMatrixMultiVar(reader, false);
                } catch (std::exception& e) {
                    out << e.what();
                    throw e;
                }
                reader.openFolder(h5folder);
            } else {    
                matrix = getSparseMatrix(reader, matrixName, true);
            }
            if (DEBUG_OUTPUT)
                out << "   reading rhs\n";
            rhs = reader.getDoubleArray(rhsName);
            numElements = rhs.size();
            if (DEBUG_OUTPUT)
                out << "   creating distribution map for " << numElements << " elements\n";
		} else {
            numElements = 0;
        }

        // communicate the number of elements among processes, so that everyone can allocate the matrix
        Comm.MaxAll(&numElements, &numGlobalElements, 1);

        // allocate the system when its dimension is known
        allocateDistributedSystem();

        // distributed loading
        if (DEBUG_OUTPUT)
            out << "   transferring matrix from Eigen to Trilinos\n";
        loadEigenMatrix(matrix, false);
        if (DEBUG_OUTPUT)
            out << "   transferring RHS from Eigen to Trilinos\n";
        loadRhs(rhs);
        if (DEBUG_OUTPUT)
            out << "   setup complete\n";

        return 0;
    }

    /**
     * Fill in A with random values (A must be pre-allocated)
     */
    int randomizeA() {
        Teuchos::TimeMonitor LocalTimer (*randTime);
        if (DEBUG_OUTPUT)
            out << "   randomizing matrix A " << "\n";
        // Seed with a real random value, if available
        std::random_device r;
        // Choose a random mean between 1 and 6
        std::default_random_engine rand(r());
        std::uniform_int_distribution<int> uniformDist(1, 10);

        ESMatrix temp(numGlobalElements, numGlobalElements);

        if (myRank == 0) {
			// rezerve for the estimated number of nonzero elements
			temp.reserve(3*numGlobalElements);
            // this scoope is here to recycle the following vector as soon as it is not required anymore
            std::vector< Eigen::Triplet<double> > tripletList;
            tripletList.reserve(3*numGlobalElements);
            for (int i = 0; i < std::min(temp.cols(), temp.rows()); ++i) {
                if ((i > 0) && (i-1 < temp.rows()))
                    tripletList.push_back(Eigen::Triplet<double>(i-1, i, 1+0*uniformDist(rand)));
                tripletList.push_back(Eigen::Triplet<double>(i, i, 1+0*uniformDist(rand)));
                if ((i < temp.rows()-1) && (i+1 < temp.rows()))
                    tripletList.push_back(Eigen::Triplet<double>(i+1, i, 1+0*uniformDist(rand)));
            }
            temp.setFromTriplets(tripletList.begin(), tripletList.end());
        }

        if (DEBUG_OUTPUT)
            out << " randomization: reserved = " << (4*numGlobalElements) << ", nonzeros = " << temp.nonZeros() << "\n";

        // as the temporary matrix is loaded, transfer it to trilineos
        loadEigenMatrix(temp);
    }

    /**
     * Load the provided Eigen sparse matrix (will make a copy)
     *  rowLength is the expected maximum number of elements per row, if a number provided is too small it will degrade performance
     * @param inputA the matrix to be copied to trilinos
     * @param oneBasedInput if the input matrix is one-based then this function can also translate it to zero based when copying data
    **/
    void loadEigenMatrix(ESMatrix &inputA, bool oneBasedInput = false) {
        Teuchos::TimeMonitor LocalTimer (*matrixImportTime);

        // a mapping that allocates all the data on process 0 only; it is used so that hdf5 can be loaded from 1 process and then redistributed
        Epetra_Map procZeroMap(numGlobalElements, (myRank==0 ? numGlobalElements : 0), indexBase, Comm);
        // local matrix A (all its data reside on process zero)
        RCP<Epetra_CrsMatrix> localA = numProcesses > 1 ? rcp(new Epetra_CrsMatrix(Copy, procZeroMap, 25)) : A;

        if (myRank==0) {
            // copy Eigen matrix to trilinos
            // Note: Trilinos matrices seem to be stored in row major format, while Eigen defaults to column major format.
            // TODO: currently this process is slow because of row-major transformation and elemnt by element copy

            if (DEBUG_OUTPUT)
                out << "   loading matrix A " << std::to_string(inputA.cols())<<"x"<<std::to_string(inputA.rows())<<"\n";
                out << "     norm1=" << (Eigen::RowVectorXd::Ones(inputA.rows())*inputA.cwiseAbs()).maxCoeff() <<
                    ", norm2=" << inputA.squaredNorm() << ", normInf=" <<
                    (inputA.cwiseAbs()*Eigen::VectorXd::Ones(inputA.cols())).maxCoeff() << "\n";

			// first copy eigen matrix to another Eigen matrix but in rowMajor order
			RowMatrix inputMat = inputA;

			// copy matrix elements one by one ...
			for (int i = 0; i < numGlobalElements; ++i) {
				for (RowMatrix::InnerIterator it(inputMat, i+oneBasedInput); it; ++it) { // it points to elements of the selected row
					double tempVal[] = {it.value()};
					global_ordinal_type tempGblInd[] = {it.col()-oneBasedInput};

                    auto errCode = localA->InsertGlobalValues (i, 1, tempVal, tempGblInd);
                    // positive error code is returned if a row has to be expanded
					if (0 > errCode) {
                        if (DEBUG_OUTPUT)
                            out << "error " << errCode << " in inserting global values\n";
						throw std::runtime_error("Failed to insert values to A("+std::to_string(i)+","+std::to_string(tempGblInd[0])+")="+
							std::to_string(tempVal[0]) + "  <- (" +
							std::to_string(it.row())+","+std::to_string(it.col())+";"+
							std::to_string(it.value())+"); A="+std::to_string(localA->NumGlobalRows())+"x"+std::to_string(localA->NumGlobalCols()));
						}
				}
			}
		}

        // call fillcomplete (no more elements will be added) on the local A matrix
        int lclerr = localA->FillComplete ();
        if (lclerr != 0) {
            std::ostringstream os;
            os << "localA.FillComplete() failed with error code " << lclerr << ".";
            throw std::runtime_error(os.str ());
        }

        //finishMatrixInitialization();

        // if more than one process is in use, local matrix has to be redistributed (otherwise it is already properly formed)
		if (numProcesses > 1) {
            if (DEBUG_OUTPUT)
                out << "     exporting matrix\n";

			Epetra_Export exporter(procZeroMap, A->OperatorDomainMap());
			//int lclerr=localA->Export(*A, exporter, Insert);
            lclerr = A->Export(*localA, exporter, Insert);
            checkForError(lclerr, "Export() failed on at least one process.");

            if (DEBUG_OUTPUT)
                out << "     finishing matrix initialization\n";
            lclerr = A->FillComplete();
            checkForError(lclerr, "FillComplete() failed on distributed matrix.");
		}
    }

    /**
     * Load the provided Eigen sparse matrix (will make a copy)
     *  rowLength is the expected maximum number of elements per row, if a number provided is too small it will degrade performance
    **/
    void loadRhs(const std::vector<double> &inputRhs) {
        Teuchos::TimeMonitor LocalTimer (*matrixImportTime);
        if (DEBUG_OUTPUT)
            out << "     loading rhs\n";

        // a mapping that allocates all the data on process 0 only; it is used so that hdf5 can be loaded from 1 process and then redistributed
        Epetra_Map procZeroMap(numGlobalElements, (myRank==0 ? numGlobalElements : 0), indexBase, Comm);
        // local matrix A (all its data reside on process zero)
        decltype(RHS) localRHS = numProcesses > 1 ? rcp(new Epetra_MultiVector(procZeroMap, 1)) : RHS;

        if (0 == myRank) {
            // copy vector elements one by one ...
            for (int i = 0; i < numMyElements; ++i) {
                // i ... index of the row that is assigned to this process
                // globalI is the global index of this row
                int globalI = myGlobalElements[i];
                localRHS->ReplaceGlobalValue(globalI, 0, inputRhs[globalI]);
            }
        }

        // if more than one process is in use, local matrix has to be redistributed (otherwise it is already properly formed)
		if (numProcesses > 1) {
            if (DEBUG_OUTPUT)
                out << "     exporting RHS\n";

			Epetra_Export exporter(procZeroMap, A->OperatorDomainMap());
			int lclerr = RHS->Export(*localRHS, exporter, Insert);
            checkForError(lclerr, "RHS Export() failed on at least one process.");

            if (DEBUG_OUTPUT)
                out << "     finishing RHS initialization\n";
		}
    }

    /**
     * Run the solver, return errorcode
     * niters number of iterations of the iterative solver
     * tolerance desired (absolute) residual tolerance
    **/
    int run() {
        Teuchos::TimeMonitor LocalTimer (*solveTime);

        if (DEBUG_OUTPUT)
            out << "Starting solver\n";

        //TEUCHOS_TEST_FOR_EXCEPTION(A == Teuchos::null, std::runtime_error, "Galeri returned a null operator A.");

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // =============================================================== //
        // B E G I N N I N G   O F   I F P A C K   C O N S T R U C T I O N //
        // =============================================================== //


        // Allocate an IFPACK factory.  The object contains no data, only
        // the Create() method for creating preconditioners.
        Ifpack Factory;

        // Create the preconditioner.  For the list of PrecType values that
        // Create() accepts, please check the IFPACK documentation.


        ParameterList List_ifpack = List.sublist("Ifpack");
        string PrecType = List_ifpack.get("Prec Type", "ILU"); //"ILUT";
        int OverlapLevel = List_ifpack.get("Overlap", 1); //1; // must be >= 0. If Comm.NumProc() == 1, it is ignored.

        RCP<Ifpack_Preconditioner> Prec = rcp (Factory.Create (PrecType, &*A, OverlapLevel));
        TEUCHOS_TEST_FOR_EXCEPTION(Prec == Teuchos::null, std::runtime_error,
                                   "IFPACK failed to create a preconditioner of type \""
                                   << PrecType << "\" with overlap level "
                                   << OverlapLevel << ".");

        if (DEBUG_OUTPUT)
            out << "   preconditioner created " << PrecType << "\n";

/*
        // Specify parameters for ILUT.  ILUT is local to each MPI process.
        List.set("fact: drop tolerance", 1e-9);
        List.set("fact: drop tolerance", 1e-4);
        List.set("fact: ilut level-of-fill", 10.0);

        // IFPACK uses overlapping Schwarz domain decomposition over all
        // participating processes to combine the results of ILU on each
        // process.  IFPACK's Schwarz method can use any of the following
        // combine modes to combine overlapping results:
        //
        // "Add", "Zero", "Insert", "InsertAdd", "Average", "AbsMax"
        //
        // The Epetra_CombineMode.h header file defines their meaning.
        List.set("schwarz: combine mode", "Add");

        // Set the parameters.
        IFPACK_CHK_ERR(Prec->SetParameters(List));
*/

        // Set the parameters alternative
        IFPACK_CHK_ERR(Prec->SetParameters(List.sublist("Ifpack Settings")));


        // Initialize the preconditioner. At this point the matrix must have
        // been FillComplete()'d, but actual values are ignored.
        IFPACK_CHK_ERR(Prec->Initialize());

        if (DEBUG_OUTPUT)
            out << "   preconditioner initialized\n";

        // Build the preconditioner, by looking at the values of the matrix.
        IFPACK_CHK_ERR(Prec->Compute());

        if (DEBUG_OUTPUT)
            out << "   preconditioner computed\n";

        // Create the Belos preconditioned operator from the Ifpack preconditioner.
        // NOTE:  This is necessary because Belos expects an operator to apply the
        //        preconditioner with Apply() NOT ApplyInverse().
        RCP<Belos::EpetraPrecOp> belosPrec = rcp (new Belos::EpetraPrecOp (Prec));

        // =================================================== //
        // E N D   O F   I F P A C K   C O N S T R U C T I O N //
        // =================================================== //

        // At this point, we need some additional objects
        // to define and solve the linear system.

        if (RHS.is_null()) {
            if (DEBUG_OUTPUT)
                out << "   RHS redefinition\n";
            // Define the left-hand side (the solution / initial guess vector)
            // and right-hand side.  The solution is in the domain of the
            // operator A, and the right-hand side is in the range of A.
            LHS = rcp (new Epetra_MultiVector (A->OperatorDomainMap (), 1));
            RHS = rcp (new Epetra_MultiVector (A->OperatorDomainMap (), 1));

            // Make the exact solution a vector of all ones.
            LHS->PutScalar(1.0);
            // Compute RHS := A * LHS.
            A->Apply(*LHS,*RHS);

            // Now randomize the right-hand side.
            RHS->Random();
        }

        // Need a Belos::LinearProblem to define a Belos solver
        RCP<Belos::LinearProblem<double,MV,OP> > problem
            = rcp (new Belos::LinearProblem<double,MV,OP>(A, LHS, RHS));

        if (DEBUG_OUTPUT)
            out << "   linear problem defined; norm1 = " << A->NormOne() << ", normInf = " << A->NormInf() << "\n";

        // Set the IFPACK preconditioner.
        //
        // We're using it as a right preconditioner.  It's better to use a
        // right preconditioner than a left preconditioner in GMRES, because
        // otherwise the projected problem will have a different residual
        // (in exact arithmetic) than the original problem.  This makes it
        // harder for GMRES to tell when it has converged.
        problem->setRightPrec (belosPrec);

        bool set = problem->setProblem();
        TEUCHOS_TEST_FOR_EXCEPTION( ! set,
                                    std::runtime_error,
                                    "*** Belos::LinearProblem failed to set up correctly! ***");

        // Create a parameter list to define the Belos solver.
        RCP<ParameterList> belosList = rcp (new ParameterList(List.sublist("Belos Solver")));
        /*
        belosList->set ("Block Size", 1);              // Blocksize to be used by iterative solver
        belosList->set ("Maximum Iterations", 1550);   // Maximum number of iterations allowed
        belosList->set ("Convergence Tolerance", 1e-15);// Relative convergence tolerance requested
        belosList->set ("Verbosity", Belos::Errors+Belos::Warnings+Belos::TimingDetails+Belos::FinalSummary );
        * */

        // Create an iterative solver manager.
        Belos::BlockGmresSolMgr<double,MV,OP> belosSolver (problem, belosList);

        if (DEBUG_OUTPUT)
            out << "   solver created; nnz = " << A->NumGlobalNonzeros() << "\n";

        // Perform solve.
        Belos::ReturnType ret = belosSolver.solve();

        // Did we converge?
        if ((0 == myRank) && (DEBUG_OUTPUT)) {
            if (ret == Belos::Converged) {
                std::cout << "Belos converged." << std::endl;
            } else {
                std::cout << "Belos did not converge." << std::endl;
            }
        }

        // Print out the preconditioner.  IFPACK preconditioner objects know
        // how to print themselves in parallel directly to std::cout.
        //std::cout << *Prec;

        return 0;
    }

    void summarizePrint() {
        Teuchos::TimeMonitor::summarize (out);
    }

    void storeOutput() {
		// copy result to vector
        ParameterList list(List.sublist("HDF5 Output"));
        std::string fname(list.get("File", "result.h5"));
        std::string varName(list.get("Variable Name", "LHS"));
        std::string varDir(list.get("Directory", "/"));

        if (DEBUG_OUTPUT)
            out << "     exporting lhs to " << fname << " /" << varName << "\n";

		// a mapping that allocates all the data on process 0 only; it is used so that hdf5 can be gadhered and stored by 1 process
		Epetra_Map procZeroMap(numGlobalElements, (myRank==0 ? numGlobalElements : 0), indexBase, Comm);
        // local matrix A (all its data reside on process zero)
        RCP<Epetra_MultiVector> localLHS = numProcesses > 1 ? rcp(new Epetra_MultiVector(procZeroMap, 1)) : LHS;

        if (DEBUG_OUTPUT)
            out << "     proc zero multivector created\n";

        if (numProcesses > 1) {
            checkForError(0, "test check for error.");
			//Epetra_Export exporter(procZeroMap, A->OperatorDomainMap());
            Epetra_Export exporter(A->OperatorDomainMap(), procZeroMap);
			int lclerr = localLHS->Export(*LHS, exporter, Insert);

            if (DEBUG_OUTPUT)
                out << "     exporter done with status " << lclerr << "\n";
            checkForError(lclerr, "LHS Export() failed on at least one process.");
		}

        // process 0 has to write back the results
        if (0 == myRank) {
            if (DEBUG_OUTPUT)
                out << "     starting to copy " << numGlobalElements << " elements" << std::endl;

			std::vector<double> localLHSVector(numGlobalElements);
            // copy vector elements one by one ...
            for (int i = 0; i < numGlobalElements; ++i) {
				localLHSVector[i] = (*localLHS)[0][i];
			}

            if (DEBUG_OUTPUT)
                out << "     elements copied\n";

			// prepare hdf5 file
			mm::HDF5IO writer(fname, mm::HDF5IO::DESTROY);
			writer.openFolder(varDir);
			//writer.setDouble2DArray(triplets);
			writer.setDoubleArray(varName.c_str(), localLHSVector);
			writer.closeFile();
        }

	}

private:
    /**
     * Finish the Trilinos matrix initialization - after fillilng it with values, call this function
     **/
    void finishMatrixInitialization() {

        // Tell the sparse matrix that we are done adding entries to it.
        int gblerr = A->FillComplete ();
        if (gblerr != 0) {
            std::ostringstream os;
            os << "A.FillComplete() failed with error code " << gblerr << ".";
            throw std::runtime_error (os.str ());
        }
    }

    /**
     * perform the standard error check (combine local error codes (min and max) to a global error code and test it against 0).
     * @param localErrorCode the local error code
     * @param errMessage the error message to be thrown (as exception of type runtime error) in case ne or more local error codes are not 0
     */
    void checkForError(int localErrorCode, const char* errMessage) {
        int gblerr = 0;
        Comm.MinAll(&localErrorCode, &gblerr, 1);
        if (gblerr != 0) {
            throw std::runtime_error(errMessage);
        }
        Comm.MaxAll(&localErrorCode, &gblerr, 1);
        if (gblerr != 0) {
            throw std::runtime_error(errMessage);
        }
    }

    void readParameterListFromFile(const char* fname, Teuchos::ParameterList& dest) {
        Teuchos::FileInputSource fileSrc(fname);
        Teuchos::XMLObject fileXML = fileSrc.getObject();

        if (fileXML.getTag() == "ParameterList") {  // && fileXML.getRequired("name") == "ILUT_Preconditioner")
            Teuchos::XMLParameterListReader ListReader;
            dest = ListReader.toParameterList(fileXML);
        }

        /*
         * a way to copy list is defined here
        if (List.name() == "ANONYMOUS") List.setName(ListToAdd.name());
        for (ParameterList::ConstIterator i = ListToAdd.begin(); i != ListToAdd.end(); ++i)
        {
        const ParameterEntry& val = ListToAdd.entry(i);
        const std::string& name = ListToAdd.name(i);
        if (val.isList()) AddSubList(List.sublist(name), ListToAdd.sublist(name));
        AddParameter(List, name, val);
        }
        * */
    }
};

int main (int argc, char *argv[]) {
#ifdef HAVE_MPI
    MPI_Init(&argc,&argv);
#endif

    try {
        Solver solver;

        solver.init(argc, argv);
        //solver.randomizeA();
        solver.run();
        solver.storeOutput();
        // solver.summarizePrint(); // Belos does this already...
    } catch (const std::exception& e) {
        std::cerr << "Exception caught: \n" << e.what() << std::endl;
    }

#ifdef HAVE_MPI
    MPI_Finalize() ;
#endif
}
