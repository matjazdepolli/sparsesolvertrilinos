# SparseSolverTrilinos


## About the solver

This is an implementation of distributed sparse system solver. It is 
implemented as a standalone program since it runs on MPI, which makes
it difficult and cumbersome to pack as a library or otherwise include
in non-MPI programs. Communication between the program that desires to 
solve a system and the solver is done with HDF5 files for data and 
XML for parameters.

Apart from MPI, the implementation is based on three libraries:

- [Eigen](eigen.tuxfamily.org/)
- [Meshless Machine]([http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Main_Page])
- [Trilinos]([https://trilinos.org/])

## Running the solver:

1. Setup Params.xml with parameters for the preconditioner, solver, input and output
2. Setup file hostfile with names and slots of hosts you wish to use
3. Prepare the input matrix A and right-hand-side vector RHS in a hdf5 file
	- Matrix A can be stored as 1-based n x 3 matrix or columns, rows, values
	- Matrix A can also be a group that contains 0-based vectors: columns, rows, values 
4. Execute script "run" (you can provide it with two optional parameters that will shadow those read from XML: input filename and h5 folder).
5. Results wil lbe written to another h5 file, that has been defined in the XML (step 1)

## Parameters


The available parameters can be found in the Trilinos documentation:

- For the solcver - [Belos Solver](https://trilinos.org/docs/dev/packages/belos/doc/html/classBelos_1_1BlockGmresSolMgr.html#acf8dab6cd5cd2e973bf8fe18ffad1d2f)
- For selecting preconditioner - [Ifpack](https://trilinos.org/docs/dev/packages/ifpack/doc/html/classIfpack.html)
- For the preconditioner - [Ifpack Settings](https://trilinos.org/docs/dev/packages/ifpack/doc/html/index.html#ifp_params)

